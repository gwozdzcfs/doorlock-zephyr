/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

#include <drivers/flash.h>
#include <storage/flash_map.h>
#include <fs/nvs.h>

extern struct nvs_fs storage_fs;

int storage_init();
