/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

/**
 * Initialize the lock solenoid
 */
int lock_solenoid_init();
/**
 * Unlock the solenoid
 */
void lock_solenoid_unlock();
/**
 * Lock the solenoid
 */
void lock_solenoid_lock();
/**
 * Unlock, then lock the solenoid
 * It is usually a good idea to use lock_solenoid_unlock_lock
 * rather than lock_solenoid_unlock because keeping the solenoid
 * in its unlocked state drives up power consumption (the lock
 * is essentially pulled in by a strong electric magnet).
 * @param time Time in milliseconds to keep unlocked
 */
void lock_solenoid_unlock_lock(int time);
