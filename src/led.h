/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

enum led_status {
	OFF = 0x00,
	ON = 0x01,
	BLINKING = 0x02
};

/**
 * Initialize the LED
 */
int led_init();
/**
 * Turn the LED on
 */
void led_on();
/**
 * Turn the LED off
 */
void led_off();
/**
 * Toggle the LED
 */
void led_toggle();
/**
 * Blink the LED once (toggle for 1 second, then toggle back)
 */
void led_blink();
/**
 * Start blinking the LED
 * @param frequency frequency in milliseconds
 */
void led_start_blinking(int frequency);
/**
 * Stop blinking the LED
 */
void led_stop_blinking();
/**
 * Get the status of the LED
 */
enum led_status led_status();
