/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

/**
 * Initialize the keyboard
 */
int kbd_init();
/**
 * scan pressed keys
 * @return all currently pressed keys
 */
char *kbd_scanKeys();
/**
 * wait until all keys are released
 */
void kbd_wait_for_release();
/**
 * wait until a specific key is released
 * @param key key to wait for
 */
void kbd_wait_for_key_release(char key);
/**
 * Get a key sequence from the keyboard.
 * A key sequence is considered complete when at least
 * one key has been pressed and all keys have been
 * released.
 * e.g. just pressing 1 will result in "1".
 * Pressing and holding 1, then pressing 2 and 3
 * and releasing everything will result in "123".
 * Every possible key is counted only once, so
 * pressing and holding 1 while pressing 2324
 * will result in 1234, not 12324.
 * @param timeout timeout in milliseconds (0: no timeout)
 * @return key sequence or NULL on timeout. Has to be freed with k_free.
 */
char *kbd_get(int timeout);
