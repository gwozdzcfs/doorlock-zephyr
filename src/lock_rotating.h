/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

/**
 * Initialize the rotating motor lock
 */
int lock_rotating_init();
/**
 * Start rotating clockwise (locking)
 */
void lock_rotating_rotate_clockwise();
/**
 * Start rotating counterclockwise (unlocking)
 */
void lock_rotating_rotate_counterclockwise();
/**
 * Stop the motor
 */
void lock_rotating_stop();
/**
 * Unlock fully (rotate until unlock position is reached)
 */
void lock_rotating_unlock();
/**
 * Lock fully (rotate until unlock position is reached)
 */
void lock_rotating_lock();
