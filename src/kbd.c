/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#include "kbd.h"
#include "led.h"
#include "compat.h"
#include <drivers/gpio.h>
#include <sys/printk.h>
#include <string.h>

static const struct gpio_dt_spec kbd[7] = {
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd0), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd1), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd2), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd3), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd4), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd5), gpios, {0}),
	GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd6), gpios, {0})
};
static const int col_pins[3] = { 6, 2, 1 };
static const int row_pins[4] = { 5, 0, 4, 3 };
static const char keys[4][3] = {
	{ '1', '2', '3' },
	{ '4', '5', '6' },
	{ '7', '8', '9' },
	{ '*', '0', '#' }
};

static char pressedKeys[13];

int kbd_init() {
	for(int i=0; i<7; i++)
		gpio_pin_configure_dt(&kbd[i], GPIO_INPUT | GPIO_PULL_UP);
	return 0;
}

char *kbd_scanKeys() {
	memset(pressedKeys, 0, 13);

	for(int i=0; i<7; i++)
		gpio_pin_configure_dt(&kbd[i], GPIO_INPUT | GPIO_PULL_UP);

	for(int row=0; row<4; row++) {
		gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_OUTPUT);
		gpio_pin_set_dt(&kbd[row_pins[row]], 0);
		for(int col=0; col<3; col++) {
			if(!gpio_pin_get_dt(&kbd[col_pins[col]]))
				pressedKeys[strlen(pressedKeys)] = keys[row][col];
		}
		gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_INPUT | GPIO_PULL_UP);
	}

	return pressedKeys;
}

void kbd_wait_for_release() {
	while(kbd_scanKeys()[0] != 0)
		k_msleep(100);
}

void kbd_wait_for_key_release(char key) {
	while(strchr(kbd_scanKeys(), key))
		k_msleep(100);
}

char *kbd_get(int timeout) {
	char *ret = k_malloc(13);
	memset(ret, 0, 13);
	char previous[13];
	*previous=0;
	while(1) {
		char *keys = kbd_scanKeys();
		if(keys && *keys) {
			if(!*ret) {
				strcpy(ret, keys);
			} else {
				for(int i=0; i<strlen(keys); i++) {
					if(!strchr(ret, keys[i]))
						ret[strlen(ret)]=keys[i];
				}
			}
			strcpy(previous, keys);
		} else if(ret[0]) {
			return ret;
		}
		k_msleep(100);
		if(timeout) {
			timeout -= 100;
			if(timeout <= 0) {
				k_free(ret);
				return NULL;
			}
		}
	}
}
