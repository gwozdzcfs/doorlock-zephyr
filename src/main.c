/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

// Use the rotating motor lock (if not set, the solenoid will be used)
// #define LOCKTYPE_ROTATING 1

// Allow changing the pin by pressing *# within
// PIN_CHANGE_TIMEOUT milliseconds of a successful unlock
#define PIN_CHANGE_TIMEOUT 10000
// Automatically re-lock RELOCK_TIMEOUT milliseconds after
// a successful unlock (if 0, the per-lock type default
// will be used -- rotating motor should usually have
// a much longer timeout. If -1, this functionality is disabled)
#define RELOCK_TIMEOUT 0
// Maximum size of a pin
#define MAX_PINSIZE 128

#include <zephyr.h>
#include <sys/printk.h>
#include <string.h>
#include "debugconsole.h"
#include "led.h"
#include "kbd.h"
#ifdef LOCKTYPE_ROTATING
#include "lock_rotating.h"
#define lock_init lock_rotating_init
#define lock_unlock lock_rotating_unlock
#define lock_lock lock_rotating_lock
#if RELOCK_TIMEOUT == 0
#undef RELOCK_TIMEOUT
#define RELOCK_TIMEOUT 25000
#endif
#else
#include "lock_solenoid.h"
#define lock_init lock_solenoid_init
#define lock_unlock lock_solenoid_unlock
#define lock_lock lock_solenoid_lock
#if RELOCK_TIMEOUT == 0
#undef RELOCK_TIMEOUT
#define RELOCK_TIMEOUT 4000
#endif
#endif
#include "storage.h"

static void relock(struct k_timer *timer) {
	lock_lock();
}
K_TIMER_DEFINE(relock_timer, relock, NULL);

/*
 * Read a new pin from the keyboard
 */
char *get_new_pin() {
	led_start_blinking(200);
	char pin[MAX_PINSIZE];
	memset(pin, 0, sizeof(pin));
	while(1) {
		char *component = kbd_get(0);
		if(!component)
			continue;
		if(component) {
			if(!strcmp(component, "*#")) {
				if(pin[0]) {
					k_free(component);
					led_stop_blinking();
					char *ret = k_malloc(strlen(pin));
					pin[strlen(pin)-1]=0; // strip trailing space
					strcpy(ret, pin);
					return ret;
				}
			}
			if((strlen(pin) + strlen(component) + 2) > sizeof(pin)) {
				led_stop_blinking();
				memset(pin, 0, sizeof(pin));
				led_blink();
				led_blink();
				led_blink();
				led_start_blinking(200);
			} else {
				strcat(pin, component);
				strcat(pin, " ");
			}
			k_free(component);
		}
	}
}

char *set_pin() {
	char *pin = get_new_pin();
	printk("New pin: ---%s---\n", pin);
	nvs_write(&storage_fs, 0, pin, strlen(pin) + 1);
	return pin;
}

void main(void)
{
	int64_t last_unlock = 0;

	int log = 0; 
        log = (0 == debugconsole_init());
	if(log) {
		printk("Debugconsole inited\n");
	}
	led_init();
	kbd_init();
	lock_init();
	storage_init();

	char pin[MAX_PINSIZE];
	int ret = nvs_read(&storage_fs, 0, pin, sizeof(pin));
	if(ret>0) {
	if(log) {
		printk("Found previous pin: ---%s---\n", pin);
	}
	} else {
		if (log) {
			printk("No previous pin\n");
		}
		char *new_pin = set_pin();
		strcpy(pin, new_pin);
		k_free(new_pin);
		if (log) {
			printk("Starting with new pin: ---%s---\n", pin);
		}
	}

	char input[MAX_PINSIZE];
	memset(input, 0, sizeof(input));
	led_blink();
	while(1) {
		if (log) {
			printk("Ready and waiting for input\n");
		}
		led_off();
		led_on();
		k_msleep(1);
		led_off();

		char *component = kbd_get(0);
		if(!component)
			continue;
		if(!strcmp(component, "*#") && last_unlock && ((k_uptime_get() - last_unlock) < PIN_CHANGE_TIMEOUT)) {
			if (log) {
				printk("PIN change requested\n");
			}
			char *new_pin = set_pin();
			strcpy(pin, new_pin);
			k_free(new_pin);
			if (log) {
				printk("Pin changed to: ---%s---\n", pin);
			}
			continue;
		}
		if((strlen(input) + strlen(component) + 2) > sizeof(input)) {
			memset(input, 0, sizeof(input));
		} else {
			strcat(input, component);
			if(!strcmp(input, pin)) {
				led_on();
				if (log) {
					printk("Correct pin received! unlocking!\n");
				}
				lock_unlock();
				if (log) {
					printk("Unlocked, proceeding\n");
				}
				led_off();
				last_unlock = k_uptime_get();
				memset(input, 0, sizeof(input));
				k_timer_start(&relock_timer, K_MSEC(RELOCK_TIMEOUT), K_NO_WAIT);
			} else {
				strcat(input, " ");
				if(strncmp(input, pin, strlen(input)))
					memset(input, 0, sizeof(input));
			}
		}
		k_free(component);
	}
}
