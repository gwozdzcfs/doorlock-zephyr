/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#include "lock_rotating.h"
#include <drivers/gpio.h>
#include <sys/printk.h>

#define DOORLOCK0_NODE DT_ALIAS(doorlock0)
#if DT_NODE_HAS_STATUS(DOORLOCK0_NODE, okay)
#define DOORLOCK0	DT_GPIO_LABEL(DOORLOCK0_NODE, gpios)
#define PIN0		DT_GPIO_PIN(DOORLOCK0_NODE, gpios)
#define FLAGS0		DT_GPIO_FLAGS(DOORLOCK0_NODE, gpios)
#else
#error "Unsupported board: doorlock0 devicetree alias is not defined"
#endif

#define DOORLOCK1_NODE DT_ALIAS(doorlock1)
#if DT_NODE_HAS_STATUS(DOORLOCK1_NODE, okay)
#define DOORLOCK1	DT_GPIO_LABEL(DOORLOCK1_NODE, gpios)
#define PIN1		DT_GPIO_PIN(DOORLOCK1_NODE, gpios)
#define FLAGS1		DT_GPIO_FLAGS(DOORLOCK1_NODE, gpios)
#else
#error "Unsupported board: doorlock1 devicetree alias is not defined"
#endif

static const struct device *motorPin0 = NULL, *motorPin1 = NULL;

void lock_stop_rotating_timer_handler(struct k_timer *timer) {
	lock_rotating_stop();
}
K_TIMER_DEFINE(lock_stop_rotating_timer, lock_stop_rotating_timer_handler, NULL);

int lock_rotating_init() {
	motorPin0 = device_get_binding(DOORLOCK0);
	if(!motorPin0) {
		printk("Can't get devicetree entry for doorlock0\n");
		return ENODEV;
	}

	motorPin1 = device_get_binding(DOORLOCK1);
	if(!motorPin1) {
		printk("Can't get devicetree entry for doorlock0\n");
		return ENODEV;
	}

	if(gpio_pin_configure(motorPin0, PIN0, GPIO_OUTPUT_ACTIVE | FLAGS0) < 0) {
		printk("Can't configure motor pin 0\n");
		return ENODEV;
	}

	if(gpio_pin_configure(motorPin1, PIN1, GPIO_OUTPUT_ACTIVE | FLAGS1) < 0) {
		printk("Can't configure motor pin 1\n");
		return ENODEV;
	}

	return 0;
}

void lock_rotating_rotate_clockwise() {
	gpio_pin_configure(motorPin1, PIN1, FLAGS1);
	gpio_pin_configure(motorPin0, PIN0, FLAGS0 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_HIGH);
}

void lock_rotating_rotate_counterclockwise() {
	gpio_pin_configure(motorPin0, PIN0, FLAGS0);
	gpio_pin_configure(motorPin1, PIN1, FLAGS1 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_HIGH);
}

void lock_rotating_stop() {
	k_timer_stop(&lock_stop_rotating_timer); // No point in leaving an auto-stop running if we've already stopped it
	gpio_pin_set(motorPin0, PIN0, 0);
	gpio_pin_set(motorPin1, PIN1, 0);
}

void lock_rotating_unlock() {
	lock_rotating_rotate_counterclockwise();
	k_timer_start(&lock_stop_rotating_timer, K_SECONDS(13), K_NO_WAIT); // It takes roughly 13 seconds to go from fully closed to fully open
}

void lock_rotating_lock() {
	lock_rotating_rotate_clockwise();
	k_timer_start(&lock_stop_rotating_timer, K_SECONDS(13), K_NO_WAIT); // It takes roughly 13 seconds to go from fully open to fully closed
}

